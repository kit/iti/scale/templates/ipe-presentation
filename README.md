# Ipe Presentation Template #

## Using the Template ##

Use the file `00-template-[A]x[B].ipe` for your preferred aspect ratio
[A]x[B].

### Name, Presentation Title, Affiliation ###

Edit the document properties (`Ctrl + Shift + P`) and adjust the
*Title* and *Author* fields, as well as `\prestitle` and
`\affiliation` commands in the *Latex preamble* appropriately.

### Language ###

In the preamble of the document properties (`Ctrl + Shift + P`), use
the `\german` command to set everything to German.  Removing the
`\german` command sets everything to English.

### Boxes With Rounded Corners (KIT-Style) ###

The template contains definitions for KIT-style boxes for the
[decorator
ipelet](https://github.com/thobl/ipelets/tree/master/decorator).

### Compilation Time ###

**update:** *It seems this issue has been resolved with newer ipe
versions. So this is probably only useful for you if you are stuck
with an older ipe version.*

The bullet points of item lists (as well as the KIT logo) are created
with TikZ.  This may slow down compilation heavily, in particular if
you have many itemize-lists.  To speed this up while editing your
presentation, you can add `\draft` in the preamble of the document
properties (`Ctrl + Shift + P`).  This will render normal (not
rounded) squares for the bullet points and blanks out the KIT logo.

Don't forget to remove the `\draft` for rendering the final result.

## Changing the Template ##

### Script for Generating the Template ###

The templates for different aspect ratios are generated using
`generate.py`.  To generate a template with a different aspect ratio,
edit the values of `slide_w` and `slide_h` in `generate.py`
accordingly.  Running `python generate.py` will create the files
`kit-presentation-[slide_w]x[slide_h].isy` and
`kit-presentation-[slide_w]x[slide_h].ipe`.  The ipe file is the
finished template, including a title page but excluding an image on
the title page.

### Snippets ###

The `snippets` folder contains parts of the template that are
independent of the aspect ratio.  Changing, something here, running
the python script, and then updating the style sheet in the template
(`Ctrl + Shift + P` or running the command `ipescript update-styles
00-template-[A]x[B].ipe`) applies the changes.

This should make it easy to change something for all aspect ratios,
i.e., just change it once in the snippets, and run the script for each
aspect ratio.

### Title Page ###

The title page cannot be controlled via the style sheet.  To change
something here, one has to edit the `generate.py` accordingly and the
recreate the `00-template-[A]x[B].ipe` files from the generated
`kit-presentation-[slide_w]x[slide_h].ipe` files by adding the picture to
the title page.
