# desired side lenghts
slide_w = 800  # 4x3
slide_h = 600  # 4x3
# slide_w = 960  # 16x9
# slide_h = 540  # 16x9
# slide_w = 880  # 16x10
# slide_h = 550  # 16x10


# margins at the sides, at the top and at the bottom
margin_rl = 40
margin_t = 107
margin_b = 45


# computing the best frame dimensions divisible by 16
frame_w = slide_w - 2 * margin_rl
frame_h = slide_h - margin_t - margin_b

frame_w = round(frame_w / 16) * 16
frame_h = round(frame_h / 16) * 16


# margin errors due to rounding
error_w = slide_w - frame_w - 2 * margin_rl
error_h = slide_h - frame_h - margin_t - margin_b
margin_rl += round(error_w / 2)
margin_t += error_h


# debugging output
print("errors:", error_w, error_h)
print("margins:", margin_rl, margin_t, margin_b)
print("frame:", frame_w, frame_h)
print("slide:", slide_w, slide_h)


# helper function for copying the contents of a file
def insert_file(filename, out):
    with open(filename) as basic:
        for line in basic:
            out.write(line)
    out.write("\n")


# write the style file
name = "kit-presentation-{}x{}".format(slide_w, slide_h)
style_file = name + ".isy"
with open(style_file, "w") as out:
    print(
        '<?xml version="1.0"?>',
        '<!DOCTYPE ipestyle SYSTEM "ipe.dtd">',
        '<ipestyle name="{}">'.format(name),
        '<symbol name="Background">',
        "<group>",
        '<text matrix="0.0429722 0 0 0.0429722 {} {}" pos="0 0" stroke="black" type="label" width="2222.1" height="1022.98" depth="0" halign="right" valign="bottom">\\kitlogo</text>'.format(
            frame_w, frame_h + 36
        ),
        '<text pos="15 -32" stroke="black" type="label" width="218.492" height="8.134" depth="2.46" valign="baseline" size="tiny">\\prestitle</text>',
        '<text pos="{} -32" stroke="black" type="label" width="291.421" height="8.134" depth="2.46" halign="right" valign="baseline" size="tiny">\\affiliation</text>'.format(
            frame_w
        ),
        '<path stroke="KITlightgray">',
        "-30 -10 m",
        "-30 -10 l",
        "{} -10 l".format(frame_w + 30),
        "{} -10 l".format(frame_w + 30),
        "h",
        "</path>",
        "</group>",
        "</symbol>",
        file=out,
        sep="\n",
    )
    insert_file("snippets/basic-style-elements.isy", out)
    print("<preamble>", file=out)
    insert_file("snippets/preamble.tex", out)
    print(
        "</preamble>",
        '<layout paper="{} {}" origin="{} {}" frame="{} {}" crop="no"/>'.format(
            slide_w, slide_h, margin_rl, margin_b, frame_w, frame_h
        ),
        '<pagenumberstyle pos="-21 -32" color="black" size="tiny" valign="baseline">\\textbf{\\ipeNumber{\\arabic{ipePage}}{\\arabic{ipePage}}}</pagenumberstyle>',
        '<titlestyle pos="0 {}" size="largebf" color="black"  valign="baseline"/>'.format(
            frame_h + 36
        ),
        "</ipestyle>",
        file=out,
        sep="\n",
    )


# write ipe file with title page
ipe_file = name + ".ipe"
with open(ipe_file, "w") as out:
    # coordinates for the frame on the title slide
    inner_x = slide_w - 45
    outer_x = slide_w - 35
    round_x = slide_w - 59
    inner_y = slide_h - 283
    outer_y = slide_h - 273
    round_y = slide_h - 297
    print(
        '<?xml version="1.0"?>',
        '<!DOCTYPE ipe SYSTEM "ipe.dtd">',
        '<ipe version="70218" creator="Ipe 7.2.24">',
        '<info created="D:20210908195518" modified="D:20210908195518" title="Algorithmische Geometrie" author="Thomas Bläsius" numberpages="yes"/>',
        "<preamble>",
        "% settings %%%%%%%%%%%%%%%",
        "\\newcommand{\\prestitle}{Thomas Bläsius~--~Algorithmische Geometrie}",
        "\\newcommand{\\affiliation}{\\inst, \\chair}",
        "\\german",
        "</preamble>",
        '<ipestyle name="kit-basic">',
        "</ipestyle>",
        '<ipestyle name="kit-colors">',
        "</ipestyle>",
        '<ipestyle name="kit-decobox">',
        "</ipestyle>",
        '<ipestyle name="{}">'.format(name),
        "</ipestyle>",
        "<page>",
        '<layer name="alpha"/>',
        '<layer name="BACKGROUND" edit="no"/>',
        '<view layers="alpha BACKGROUND" active="alpha"/>',
        '<text layer="BACKGROUND" matrix="0.0643153 0 0 0.0643153 0 {}" pos="0 424.645" stroke="black" type="label" width="2222.1" height="1022.98" depth="0" valign="baseline">\\kitlogo</text>'.format(
            slide_h - 170
        ),
        '<text layer="alpha" matrix="1 0 0 1 0 10" pos="0 {}" stroke="black" type="label" width="331.566" height="19.3606" depth="5.9052" valign="baseline" size="large">\\textbf{{Algorithmische Geometrie}}</text>'.format(
            slide_h - 200
        ),
        '<text matrix="1 0 0 1 0 -38" pos="0 {}" stroke="black" type="label" width="86.1893" height="14.1059" depth="4.107" valign="baseline" size="small">\\textbf{{Template}}</text>'.format(
            slide_h - 200
        ),
        '<text matrix="1 0 0 1 0 -59" pos="0 {}" stroke="black" type="label" width="42.0202" height="14.5343" depth="4.107" valign="baseline" size="small">\\textbf{{(ipe)}}</text>'.format(
            slide_h - 200
        ),
        '<path layer="BACKGROUND" matrix="1 0 0 1 -5 0" fill="white">',
        "{} {} m".format(round_x, inner_y),
        "-25 {} l".format(inner_y),
        "-25 4 l",
        "14 0 0 14 -11 4 -11 -10 a",
        "{} -10 l".format(inner_x),
        "{} {} l".format(inner_x, round_y),
        "14 0 0 14 {} {} {} {} a".format(round_x, round_y, round_x, inner_y),
        "h",
        "-35 {} m".format(outer_y),
        "-35 -45 l",
        "{} -45 l".format(outer_x),
        "{} {} l".format(outer_x, outer_y),
        "h",
        "</path>",
        '<path matrix="1 0 0 1 -5 0" stroke="KITlightgray">',
        "{} {} m".format(round_x, inner_y),
        "-25 {} l".format(inner_y),
        "-25 4 l",
        "14 0 0 14 -11 4 -11 -10 a",
        "{} -10 l".format(inner_x),
        "{} {} l".format(inner_x, round_y),
        "14 0 0 14 {} {} {} {} a".format(round_x, round_y, round_x, inner_y),
        "h",
        "</path>",
        '<text matrix="1 0 0 1 -5 0" pos="{} -34" stroke="black" type="label" width="102.533" height="12.9293" depth="0.1998" halign="right" valign="baseline" size="footnote">\\textbf{{www.kit.edu}}</text>'.format(
            inner_x
        ),
        '<text matrix="0.832699 0 0 0.832699 -9.18252 -3.91074" pos="-25 -34" stroke="black" type="label" width="314.656" height="8.15184" depth="2.442" valign="baseline" size="tiny">\\helmholz</text>',
        "</page>",
        "<page>",
        '<layer name="alpha"/>',
        '<view layers="alpha" active="alpha"/>',
        "</page>",
        "</ipe>",
        file=out,
        sep="\n",
    )

# update style sheets
import os

os.system("ipescript update-styles {}".format(ipe_file))
